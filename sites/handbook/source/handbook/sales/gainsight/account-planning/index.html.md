---
layout: handbook-page-toc
title: "Using Gainsight within Sales"
description: "The key aspects of how Sales uses Gainsight to drive success for customers."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

*For an overview of Gainsight, and information about how to login, please refer to the [Gainsight Overview Page](/handbook/sales/gainsight/).*

### Gainsight in Salesforce

Once logged into Salesforce, navigate to the Account or Opportunity page. Scroll down to the Gainsight widget.  The Customer 360 page will be the default view and is customized to show high-level account data and navigational tabs specific to Account Planning. 

### Creating an Account Plan

1. From the Gainsight widget, use the top navigation bar to select the **Success Plan tab**.
1. In the right corner select **New Success Plan**.
1. Name your success plan indicating Land or Expand and year(s).
1. The **Type** will be Account Planning.
1. Add a due date. This will evolve as the plan evolves.
1. Add a template to the Account Plan by using the ellipsis menu located in the gray bar on the right side of the page. The [Land Plan](/handbook/sales/account-planning/#land-account-plans) and [Expand Plan](/handbook/sales/account-planning/#expand-account-plans) templates will provide a guided action items and next steps.
1. To learn about other plan types (Opportunity Plan, Command Plan, Success Plan), review [this page](/handbook/sales/account-planning/#what-is-the-difference-between-an-account-plan-and-a-success-plan).
   

### Locating an Existing Account Plan

1. From the Gainsight widget, use the top navigation bar to select the **Success Plan tab**.
1. In the gray bar towards the top of the widget, use the **More Plans arrow** to view the existing plans.

### Account Plan Info

1. The default view of an Account Plan will be the Plan Info section.
1. Plan Info is comprised of the Account Profile, Execution Plan and Go-To-Market Plan. 
1. Add or edit content to the Plan Info by using the pencil icon located to the right of each text box. 
1. For further detail on the methodology behind the individual components, [click here](/handbook/sales/account-planning/#account-profile).

### Relationship and Influence Mapping

1. From the Gainsight widget, use the top navigaion bar to select the **Contacts tab**.
1. Use the drag and drop function to add contacts to the map or use the Add Person button located to the right of the widget. 
1. Add further detail (Influence, GitLab Role, Manager) to the contact card by double clicking in the card or using the ellipsis menu.
1. Check out [this article](https://support.gainsight.com/Gainsight_NXT/07360/People_Maps/Build_People_Maps#Business_Use_Cases) from Gainsight on building maps as well as [this enablement](/handbook/sales/account-planning/#relationship-and-influence-mapping) page.

### Whitespace Mapping

1.  The Gainsight widget for Account Planning has two tabs for whitespace mapping. Whitespace (Use Case) and Whitespace (Use Case Visual).
      - **Whitespace (Use Case):** This is where you will enter the data. It will flow back into Salesforce but cannot be edited in Salesforce at this time. 
      - **Whitespace (Use Case Visual):** After data has been entered, use this tab to see a visual representation of the data.  There are multiple views from which to choose. 
1. Reference this [Whitespace Mapping page](/handbook/sales/account-planning/#white-space-mapping) for further detail on the methodology.


### Company Intelligence

Company Intelligence allows users to stay up-to-date on their key customers and prospects and can be located in the Company Intelligence tab. 
   - For a complete overview of Company Intelligence, read [this article](https://support.gainsight.com/SFDC_Edition/Company_Intelligence/About/Company_Intelligence_Overview?mt-draft=true#Overview).


### Sponsor Tracking

1.  From the Gainsight widget, use the top navigation bar to select the **Sponsor Tracking** tab.
1.  Click **+Sponsor** to choose a contact from the list or add a new contact by using the search feature. 
1.  In the action column, click the link and enter the LinkedIn URL to start tracking.
1.  Private, Premium LinkedIn profiles can't be tracked at this time (May 2021).
      - Profile with special characters cannot be tracked unless a request is submitted to Gainsight Customer Support.
1. For more detail on how Sponsor Tracking works, please [read this article](https://support.gainsight.com/SFDC_Edition/View_More_Categories/Sponsor_Tracking/User_Guides/How_to_Use_Sponsor_Tracking#Changes_in_Tracked_Contacts) from Gainsight. 

### Video Enablement 

Coming Soon! 
