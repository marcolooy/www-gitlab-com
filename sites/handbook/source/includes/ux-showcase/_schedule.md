[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Host                 |
| ---------- | ---------------      |
| 2021-05-12 | Rayana               |
| 2021-05-26 | Taurie               |
| 2021-06-09 | Mike                 |
| 2021-06-23 | Jacki                |
| 2021-07-07 | Justin               |
| 2021-07-21 | APAC/Europe (Rayana) |
| 2021-08-04 | Marcel               |
| 2021-08-18 | Rayana               |
